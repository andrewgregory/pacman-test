#include "pactest.c/pactest.c"
#include "pactest.c/ptserve.c"
#include "tap.c/tap.c"

#include <stdio.h>
#include <limits.h>
#include <unistd.h>

pt_env_t *pt = NULL;
ptserve_t *ptserve = NULL;
char *url = NULL, *path = NULL;

void respond(ptserve_message_t *request) {
    alpm_list_t *headers = NULL;
    headers = alpm_list_append(&headers,
            "Content-Disposition: filename=\"../foo\"");
    ptserve_send_bytes(request->socket_fd, headers, "foobar", 6);
}

void cleanup() {
    ptserve_free(ptserve);
    pt_cleanup(pt);
    free(url);
    free(path);
}

int main(int argc, char *argv[]) {
    tap_assert( atexit(cleanup) == 0 );
    tap_assert( pt = pt_new(NULL) );
    tap_assert( pt_initialize(pt, NULL) );

    tap_assert( ptserve = ptserve_new() );
    ptserve->response_cb = respond;
    tap_assert( ptserve_serve_async(ptserve) == 0 );

    tap_assert( url = pt_asprintf("%s/bar", ptserve->url) );

    tap_plan(5);

    tap_ok( (path = alpm_fetch_pkgurl(pt->handle, url)) != NULL,
            "fetch_pkgurl succeeds" );
    tap_is_str( path, pt_path(pt, "var/cache/pacman/pkg/foo"), "returned path" );
    tap_ok( faccessat(pt->rootfd, "var/cache/pacman/pkg/foo", F_OK, 0) == 0,
            "foo exists in cache" );
    tap_ok( faccessat(pt->rootfd, "var/cache/pacman/foo", F_OK, 0) != 0,
            "foo does not exist outside cache" );
    tap_is_int( errno, ENOENT, "errno == ENOENT" );

    return tap_finish();
}
